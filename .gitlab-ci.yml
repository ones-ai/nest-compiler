stages:
  # - check
  - build
  - publish
  - test

variables:
  GIT_SUBMODULE_STRATEGY: recursive
  GIT_DEPTH: 1000

workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS
      when: never
    - if: $CI_COMMIT_BRANCH

# check-dco:
#   stage: check
#   image: onesai1/dco-check:latest
#   script:
#     - check-dco

.builds:
  stage: build
  image: onesai1/nest-compiler-sdk:1.0.0
  before_script:
    - echo $CI_PROJECT_DIR
    - pwd
    - ls -al
    - mkdir build_ci
    - cd build_ci
    - export TVM_HOME=$CI_PROJECT_DIR/tvm
    - export DMLC_CORE=$CI_PROJECT_DIR/tvm/3rdparty/dmlc-core
    - export TVM_BUILD_PATH=$CI_PROJECT_DIR/build_ci/tvm
    - export TVM_LIBRARY_PATH=$CI_PROJECT_DIR/build_ci/tvm
    - export PATH=$CI_PROJECT_DIR/build_ci/tvm:$PATH
    - export PYTHONPATH=$TVM_HOME/python:$PYTHONPATH
  tags:
    - etri-compute

build:
  extends: .builds
  script:
    - cmake -G Ninja ../ -DCMAKE_BUILD_TYPE=Release -DNESTC_WITH_EVTA=ON -DNESTC_EVTA_BUNDLE_TEST=ON -DGLOW_WITH_BUNDLES=ON
    - cat ../nestc_tests/nestc_vta_bundle_tests.txt | xargs -I {} ninja {}
    - cat ../nestc_tests/nestc_build_tests.txt | xargs -I {} ninja {}
    - ninja tvm
    - ninja check_nestc
    - ls -al

build_from_aws:
  extends: .builds
  script:
    - cmake -G Ninja ../ -DCMAKE_BUILD_TYPE=Release -DNESTC_WITH_EVTA=ON -DNESTC_EVTA_BUNDLE_TEST=ON -DGLOW_WITH_BUNDLES=ON -DNESTC_USE_PRECOMPILED_BUNDLE=ON -DNESTC_USE_PRECOMPILED_BUNDLE_FROM_AWS=ON
    - cat ../nestc_tests/nestc_vta_bundle_tests.txt | xargs -I {} ninja {}
    - cat ../nestc_tests/nestc_build_tests.txt | xargs -I {} ninja {}
    - ninja tvm
    - ninja check_nestc
  allow_failure: true

build-publish:
  extends: .builds
  script:
    - cmake -G Ninja ../ -DCMAKE_BUILD_TYPE=Release -DNESTC_WITH_EVTA=ON -DNESTC_EVTA_BUNDLE_TEST=ON -DGLOW_WITH_BUNDLES=ON -DNESTC_OPENCL_TEST=ON -DNESTC_RELAY_TARGET_MALI=ON -DNESTC_TARGET_AARCH64=ON
    - ninja vtaMxnetResnet18Bundle
    - ninja resnet18RelayOpenCLBundleCPPGen
    - ninja resnet18RelayAarch64BundleCPPGen
    - ninja vtaMxnetResnet18PartitionBundleNet
    - ninja vtaMxnetResnet18RelayEVTAPartitionBundleCPPGen
    - ninja vtaVGG16Cifar10Bundle
    - cd ..
    - mkdir -p build_ci2
    - cd build_ci2
    - cmake -G Ninja ../ -DCMAKE_BUILD_TYPE=Release -DNESTC_WITH_EVTA=ON -DNESTC_EVTA_BUNDLE_TEST=ON -DGLOW_WITH_BUNDLES=ON -DNESTC_OPENCL_TEST=ON -DNESTC_RELAY_TARGET_MALI=ON -DNESTC_TARGET_AARCH64=ON -DNESTC_EVTA_PROFILE_AUTOTUNE=ON
    - ninja vtaResConv1TestBundleProfiling
    - ninja vtaResConv2TestBundleProfiling
    - ninja vtaResConv3TestBundleProfiling
    - ninja vtaResConv4TestBundleProfiling
    - ninja vtaResConv5TestBundleProfiling
    - ninja vtaResConv6TestBundleProfiling
    - ninja vtaResConv7TestBundleProfiling
    - ninja vtaResConv8TestBundleProfiling
    - ninja vtaResConv9TestBundleProfiling
    - ninja vtaResConv10TestBundleProfiling
    - cd ..
    - cp build_ci2/* build_ci/ -r
  artifacts:
    paths:
      - build_ci/vta/bundles
      - build_ci/examples

build-autotune-publish:
  extends: .builds
  script:
    - cmake -G Ninja ../ -DCMAKE_BUILD_TYPE=Release -DNESTC_WITH_EVTA=ON -DNESTC_EVTA_BUNDLE_TEST=ON -DGLOW_WITH_BUNDLES=ON -DNESTC_OPENCL_TEST=ON -DNESTC_RELAY_TARGET_MALI=ON -DNESTC_TARGET_AARCH64=ON -DNESTC_EVTA_PROFILE_AUTOTUNE=ON
    - ninja vtaResConv1TestBundleProfiling
    - ninja vtaResConv2TestBundleProfiling
    - ninja vtaResConv3TestBundleProfiling
    - ninja vtaResConv4TestBundleProfiling
    - ninja vtaResConv5TestBundleProfiling
    - ninja vtaResConv6TestBundleProfiling
    - ninja vtaResConv7TestBundleProfiling
    - ninja vtaResConv8TestBundleProfiling
    - ninja vtaResConv9TestBundleProfiling
    - ninja vtaResConv10TestBundleProfiling
  when: manual
  artifacts:
    paths:
      - build_ci/vta/bundles
      - build_ci/examples

build-multievta:
  extends: .builds
  script:
    - cmake -G Ninja ../ -DCMAKE_BUILD_TYPE=Release -DNESTC_WITH_EVTA=ON -DNESTC_EVTA_BUNDLE_TEST=ON -DGLOW_WITH_BUNDLES=ON -DNESTC_EVTA_MULTI=ON
    - ninja check_multievta

build-vtaconv:
  extends: .builds
  script:
    - cmake -G Ninja ../ -DCMAKE_BUILD_TYPE=Release -DNESTC_WITH_EVTA=ON -DNESTC_EVTA_BUNDLE_TEST=ON -DGLOW_WITH_BUNDLES=ON -DNESTC_EVTA_GRAPH_OPT=ON
    - cat ../nestc_tests/nestc_vta_bundle_tests.txt | xargs -I {} ninja {}
    - cat ../nestc_tests/nestc_build_tests.txt | xargs -I {} ninja {}
    - ninja check_nestc

build-openedge:
  extends: .builds
  script:
    - cmake -G Ninja ../ -DCMAKE_BUILD_TYPE=Release -DNESTC_WITH_EVTA=ON -DNESTC_EVTA_BUNDLE_TEST=ON -DGLOW_WITH_BUNDLES=ON -DNESTC_WITH_OPENEDGE=ON -DNESTC_WITH_ENLIGHT=ON
    - ninja resnet50_openedgeBundle; ninja enlightConvBundle; ninja onnxYamlParserValTestBunddle

build-debug:
  extends: .builds
  script:
    - cmake -G Ninja ../ -DCMAKE_BUILD_TYPE=Debug -DGLOW_WITH_BUNDLES=ON -DNESTC_WITH_EVTA=ON -DNESTC_EVTA_BUNDLE_TEST=ON -DGLOW_VTA_INTERPRETER_FUSION=ON -DGLOW_VTA_FUSION=ON
    - cat ../nestc_tests/nestc_build_tests.txt | xargs -I {} ninja {}
    - ninja tvm
    - ninja check_nestc

build-without-VTA:
  extends: .builds
  script:
    - cmake -G Ninja ../ -DCMAKE_BUILD_TYPE=Release -DGLOW_WITH_BUNDLES=ON
    - ninja all
  allow_failure: true

build_bnn:
  extends: .builds
  script:
    - cmake -G Ninja ../ -DCMAKE_BUILD_TYPE=Release -DNESTC_WITH_EVTA=ON -DNESTC_BNN=ON
    - ninja check_nestc

build-all:
  extends: .builds
  script:
    - cmake -G Ninja ../ -DCMAKE_BUILD_TYPE=Release -DNESTC_EVTA_BUNDLE_TEST=ON -DGLOW_WITH_BUNDLES=ON -DNESTC_WITH_EVTA=ON -DGLOW_WITH_STC=ON
    - ninja all
  allow_failure: true

test-all:
  extends: .builds
  stage: test
  script:
    - cmake -G Ninja ../ -DCMAKE_BUILD_TYPE=Release -DGLOW_WITH_BUNDLES=ON -DGLOW_WITH_VTA=ON -DGLOW_WITH_STC=ON
    - ninja all
    - ninja check
  when: manual
  allow_failure: true

test-board:
  stage: test
  before_script:
    - echo $CI_PROJECT_DIR
    - pwd
    - ls -al
    - mkdir build_ci
    - cd build_ci
    - export TVM_HOME=$CI_PROJECT_DIR/tvm
    - export DMLC_CORE=$CI_PROJECT_DIR/tvm/3rdparty/dmlc-core
    - export TVM_BUILD_PATH=$CI_PROJECT_DIR/build_ci/tvm
    - export TVM_LIBRARY_PATH=$CI_PROJECT_DIR/build_ci/tvm
    - export PATH=$CI_PROJECT_DIR/build_ci/tvm:$PATH
    - export PYTHONPATH=$TVM_HOME/python:$PYTHONPATH
  script:
    - cmake -DNESTC_WITH_EVTA=ON -DLLVM_DIR=/usr/lib/llvm-8.0/lib/cmake/llvm -DCMAKE_BUILD_TYPE=Release -DNESTC_USE_VTASIM=OFF -DVTA_RESNET18_WITH_SKIPQUANT0=ON -DNESTC_EVTA_RUN_ON_ZCU102=ON -DNESTC_USE_PRECOMPILED_BUNDLE=ON -DNESTC_EVTA_RUN_WITH_GENERIC_BUNDLE=ON ..
    # - sudo make check_zcu102
  tags:
    - etri-board
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS
      when: never
    - if: $CI_COMMIT_BRANCH
      when: manual

#build-newton:
#  extends: .builds
#  script:
#    - update-alternatives --set cc /usr/bin/gcc; update-alternatives --set c++ /usr/bin/g++
#    - cmake -G Ninja ../ -DCMAKE_BUILD_TYPE=Release -DNESTC_WITH_EVTA=OFF -DNESTC_EVTA_BUNDLE_TEST=ON -DGLOW_WITH_BUNDLES=ON -DGLOW_WITH_BUNDLES=ON -DNESTC_WITH_NEWTON=ON
#    - cat ../nestc_tests/nestc_newton_tests.txt | xargs -I {} ninja {}

publish:
  stage: publish
  image:
    name: banst/awscli
    entrypoint: [""]
  script:
    - aws configure set region  ap-northeast-2
    - aws s3 rm s3://$S3_BUCKET/ --recursive
    - aws s3 cp build_ci s3://$S3_BUCKET/ --recursive
  when: manual
  allow_failure: true
